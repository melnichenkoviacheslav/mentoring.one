﻿using Microsoft.EntityFrameworkCore;
using Net.Mentoring.One.Data.Model.MentoringData;

namespace Net.Mentoring.One.Data.Repository
{
    public class NorthWindDbContext : DbContext
    {
        public NorthWindDbContext(DbContextOptions<NorthWindDbContext> options) : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            
            modelBuilder.Entity<Product>()
                .Property(f => f.Id)
                .ValueGeneratedOnAdd();
        }
    }
}