﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Net.Mentoring.One.Data.Model.MentoringData;

namespace Net.Mentoring.One.Data.Repository
{
    public class NorthWindRepository : INorthWindRepository
    {
        private readonly NorthWindDbContext _dbContext;

        public NorthWindRepository(NorthWindDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<IEnumerable<Product>> GetProductsAsync()
        {
            return await _dbContext.Products.ToListAsync();
        }

        public async Task<IEnumerable<Category>> GetCategoriesAsync()
        {
            return await _dbContext.Categories.ToListAsync();
        }

        public async Task<Category> GetCategoryAsync(int id)
        {
            return await _dbContext.Categories.SingleAsync(c => c.Id == id);
        }

        public async Task<Product> AddProduct(Product inputProduct)
        {
            var product = _dbContext.Products.Attach(inputProduct);
            _dbContext.Entry(inputProduct).State = EntityState.Added;
            await Save();

            return product.Entity;
        }

        public async Task<bool> Save()
        {
            return await _dbContext.SaveChangesAsync() >= 0;
        }

        public async Task<IEnumerable<Supplier>> GetSuppliersAsync()
        {
            return await _dbContext.Suppliers.ToListAsync();
        }

        public async Task<Product> GetProductAsync(int id)
        {
            return await _dbContext.Products.SingleAsync(c => c.Id == id);
        }

        public async Task<Supplier> GetSupplierAsync(int id)
        {
            return await _dbContext.Suppliers.SingleAsync(c => c.Id == id);
        }

        public async Task<Product> UpdateProduct(Product product)
        {
            _dbContext.Products.Attach(product);
            _dbContext.Entry(product).State = EntityState.Modified;
            await Save();
            return product;
        }

        public async Task<Category> UpdateCategoryAsync(Category category)
        {
            _dbContext.Categories.Attach(category);
            _dbContext.Entry(category).State = EntityState.Modified;
            await Save();
            return category;
        }

        public async Task<byte[]> GetCategoryImageAsync(string categoryName)
        {
            var picture = await _dbContext.Categories.SingleOrDefaultAsync(c => c.CategoryName == categoryName);
            return picture?.Picture;
        }

        public async Task<byte[]> SetCategoryImageAsync(string categoryName, byte[] image)
        {
            var category = await _dbContext.Categories.SingleOrDefaultAsync(c => c.CategoryName == categoryName);
            category.Picture = image;
            await Save();
            return category.Picture;
        }

        public async Task DeleteProduct(Product product)
        {
            _dbContext.Products.Remove(product);
            await Save();
        }
    }
}