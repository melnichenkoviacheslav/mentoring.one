﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Net.Mentoring.One.Data.Model.MentoringData;

namespace Net.Mentoring.One.Data.Repository
{
    public interface INorthWindRepository
    {
        Task<IEnumerable<Product>> GetProductsAsync();
        Task<IEnumerable<Category>> GetCategoriesAsync();
        Task<Category> GetCategoryAsync(int id);
        Task<Product> AddProduct(Product product);
        Task<bool> Save();
        Task<IEnumerable<Supplier>> GetSuppliersAsync();
        Task<Product> GetProductAsync(int id);
        Task<Supplier> GetSupplierAsync(int id);
        Task<Product> UpdateProduct(Product product);
        Task<Category> UpdateCategoryAsync(Category category);
        Task<byte[]> GetCategoryImageAsync(string categoryName);
        Task DeleteProduct(Product product);
        Task<byte[]> SetCategoryImageAsync(string imageName, byte[] image);
    }
}