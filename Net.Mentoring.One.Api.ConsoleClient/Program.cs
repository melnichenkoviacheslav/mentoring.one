﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Net.Mentoring.One.Domain.Model;

namespace Net.Mentoring.One.Api.ConsoleClient
{
    static class Program
    {
        static readonly HttpClient Client = new HttpClient();

        static void ShowProduct(DomainProduct product)
        {
            Console.WriteLine($"Name: {product.ProductName}\tPrice: " +
                              $"{product.UnitPrice}\tCategory: {product.CategoryId}");
        }

        static async Task<int> CreateProductAsync(DomainProduct product)
        {
            HttpResponseMessage response = await Client.PostAsJsonAsync(
                "api/products", product);
            response.EnsureSuccessStatusCode();

            var createdProduct = await response.Content.ReadAsAsync<DomainProduct>();

            return createdProduct.Id;
        }

        static async Task<List<DomainProduct>> GetProductsAsync()
        {
            var products = new List<DomainProduct>();
            var response = await Client.GetAsync($"api/products");
            if (response.IsSuccessStatusCode)
            {
                products = await response.Content.ReadAsAsync<List<DomainProduct>>();
            }

            return products;
        }

        static async Task<DomainProduct> UpdateProductAsync(DomainProduct product)
        {
            HttpResponseMessage response = await Client.PutAsJsonAsync(
                $"api/products", product);
            response.EnsureSuccessStatusCode();

            // Deserialize the updated product from the response body.
            product = await response.Content.ReadAsAsync<DomainProduct>();
            return product;
        }

        static async Task<HttpStatusCode> DeleteProductAsync(int id)
        {
            HttpResponseMessage response = await Client.DeleteAsync(
                $"api/products?id={id}");
            return response.StatusCode;
        }

        static void Main()
        {
            RunAsync().GetAwaiter().GetResult();
        }

        static async Task RunAsync()
        {
            // Update port # in the following line.
            Client.BaseAddress = new Uri("http://localhost:6000/api");
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                // Create a new product
                Console.WriteLine("Creating new product...");
                var product = new DomainProduct
                {
                    ProductName = "Gizmo",
                    UnitPrice = 100,
                    CategoryId = 1,
                    SupplierId = 1,
                    Discontinued = 23,
                    QuantityPerUnit = "Quantity",
                    UnitsInStock = 3,
                    UnitsOnOrder = 5,
                    ReorderLevel = 9
                };

                var productId = await CreateProductAsync(product);
                Console.WriteLine($"Product {productId} created");
                Console.WriteLine();

                // Get the product
                product = (await GetProductsAsync()).Single(product => product.Id == productId);
                ShowProduct(product);

                // Update the product
                Console.WriteLine("Updating price...");
                product.UnitPrice = 80;
                await UpdateProductAsync(product);
                Console.WriteLine();


                // Get the updated product
                product = (await GetProductsAsync()).Single(product => product.Id == productId);
                ShowProduct(product);
                Console.WriteLine();

                // Delete the product
                Console.WriteLine("Deleting new product...");
                var statusCode = await DeleteProductAsync(product.Id);
                Console.WriteLine($"Deleted (HTTP Status = {(int)statusCode})");
                Console.WriteLine();

                Console.WriteLine("Current products:");
                var allProducts = await GetProductsAsync();
                Console.WriteLine(allProducts.Select(p => p.ProductName).Aggregate((x, y) => $"{x}{Environment.NewLine}{y}"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }
}