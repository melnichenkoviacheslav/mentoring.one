using NSwag;
using NSwag.CodeGeneration.CSharp;
using System;
using System.Net.Http;
using Xunit;

namespace Net.Mentoring.One.Api.UnitTests
{
    public class UnitTest1
    {
        [Fact]
        public async void GenerateClient()
        {
            System.Net.WebClient wclient = new System.Net.WebClient();

            var document =
                await OpenApiDocument.FromJsonAsync(
                    wclient.DownloadString("https://localhost:6001/swagger/v1/swagger.json"));

            wclient.Dispose();

            var settings = new CSharpClientGeneratorSettings
            {
                ClassName = "GeneratedClient",
                CSharpGeneratorSettings =
                {
                    Namespace = "Net.Mentoring.One.Api.UnitTests"
                }
            };

            var generator = new CSharpClientGenerator(document, settings);
            var code = generator.GenerateFile();
        }

        [Fact]
        public async void GetProducts()
        {
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("https://localhost:6001");
            var client = new GeneratedClient(httpClient);
            var result = await client.GetProductsAsync();
            Assert.True(result.Count > 5);
        }
    }
}