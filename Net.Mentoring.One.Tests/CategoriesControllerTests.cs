using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Net.Mentoring.One.Controllers;
using Net.Mentoring.One.Data.Model.MentoringData;
using Net.Mentoring.One.Data.Repository;
using Net.Mentoring.One.Domain.NorthWindOrchestrator;
using Net.Mentoring.One.Mapping.Profiles;
using Net.Mentoring.One.View.Model.Categories;
using Xunit;

namespace Net.Mentoring.One.UnitTests
{
    public class CategoriesControllerTests
    {
        private readonly IMapper _mapper;

        public CategoriesControllerTests()
        {
            if (_mapper == null)
            {
                var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new CategoriesProfile()); });
                _mapper = mappingConfig.CreateMapper();
            }
        }

        [Fact]
        public async Task GetCategories_Returns_A_ViewResult_With_A_GetCategoryViewModel()
        {
            // Arrange
            var id = 1;
            var fixture = new Fixture();
            var category = fixture.Create<Category>();

            var mockLogger = new Mock<ILogger<CategoriesController>>();
            var mockRepo = new Mock<INorthWindRepository>();
            var mockOrchestrator = new NorthWindOrchestrator(mockRepo.Object, _mapper);

            mockRepo
                .Setup(repo => repo.GetCategoryAsync(id))
                .ReturnsAsync(category);

            var controller = new CategoriesController(
                mockOrchestrator,
                mockLogger.Object,
                _mapper);

            // Act
            var result = await controller.GetCategoryAsync(id);

            // Assert
            result.Should().NotBeNull();
            result.Should().BeOfType<ViewResult>();

            var viewResult = result as ViewResult;
            viewResult.Should().NotBeNull();

            var model = viewResult.ViewData.Model as GetCategoryViewModel;

            model.Should().NotBeNull();
            model.Description.Should().NotBeEmpty();
            model.Id.Should().NotBe(0);
            model.CategoryName.Should().NotBeEmpty();
        }
    }
}