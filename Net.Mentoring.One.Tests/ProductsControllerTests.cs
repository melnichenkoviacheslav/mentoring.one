using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Net.Mentoring.One.Controllers;
using Net.Mentoring.One.Data.Model.MentoringData;
using Net.Mentoring.One.Data.Repository;
using Net.Mentoring.One.Domain.NorthWindOrchestrator;
using Net.Mentoring.One.Mapping.Profiles;
using Net.Mentoring.One.View.Model.Products;
using Xunit;

namespace Net.Mentoring.One.UnitTests
{
    public class ProductsControllerTests
    {
        private static IMapper _mapper;
        private static IConfiguration _configuration;
        private static Mock<ILogger<ProductsController>> _mockLogger;
        private static Mock<INorthWindRepository> _mockRepo;
        private static NorthWindOrchestrator _orchestrator;
        private static Fixture _fixture;
        private static ProductsController _controller;
        private const int Id = 1;

        public ProductsControllerTests()
        {
            SetupMapper();
            SetupConfiguration();
            SetupMocks();
        }

        [Fact]
        public async Task GetProduct_Returns_A_ViewResult_With_A_GetProductViewModel()
        {
            // Arrange
            // Act
            var result = await _controller.GetProduct(Id);

            // Assert
            result.Should().NotBeNull();
            result.Should().BeOfType<ViewResult>();

            var viewResult = result as ViewResult;
            viewResult.Should().NotBeNull();

            var model = viewResult.ViewData.Model as GetProductViewModel;
            model.Should().NotBeNull();
        }

        [Fact]
        public async Task GetProducts_Returns_A_ViewResult_With_A_GetProductViewModel()
        {
            // Arrange
            // Act
            var result = await _controller.GetProducts();

            // Assert
            result.Should().NotBeNull();
            result.Should().BeOfType<ViewResult>();

            var viewResult = result as ViewResult;
            viewResult.Should().NotBeNull();

            var model = viewResult.ViewData.Model as IEnumerable<GetProductViewModel>;

            model.Should().NotBeNull();
            model.Should().HaveCount(2);
        }

        [Fact]
        public async Task AddProduct_Empty_Returns_A_ViewResult_With_A_GetProductViewModel()
        {
            // Arrange
            // Act
            var result = await _controller.AddProduct();

            // Assert
            result.Should().NotBeNull();
            result.Should().BeOfType<ViewResult>();

            var viewResult = result as ViewResult;
            viewResult.Should().NotBeNull();

            var model = viewResult.ViewData.Model as PostProductViewModel;
            model.Should().NotBeNull();
        }

        [Fact]
        public async Task AddProduct_New_Returns_A_RedirectToActionResult()
        {
            // Arrange
            var product = _fixture
                .Build<ProductViewModel>()
                .Without(m => m.UnitPrice)
                .Without(m => m.UnitsInStock)
                .Create();

            // Act
            var result = await _controller.AddProduct(product);

            // Assert
            result.Should().NotBeNull();
            result.Should().BeOfType<RedirectToActionResult>();

            var viewResult = result as RedirectToActionResult;
            viewResult.Should().NotBeNull();
            viewResult.ActionName.Should().NotBeNull();
            _mockRepo.Invocations.Should()
                .HaveCount(1)
                .And
                .Subject.Single().Method.Name
                .Should().Be("AddProduct");
        }

        [Fact]
        public async Task UpdateProduct_Returns_A_RedirectToActionResult()
        {
            // Arrange
            var product = _fixture
                .Build<GetProductViewModel>()
                .Without(m => m.UnitPrice)
                .Without(m => m.UnitsInStock)
                .Create();

            // Act
            var result = await _controller.UpdateProduct(Id, product);

            // Assert
            result.Should().NotBeNull();
            result.Should().BeOfType<RedirectToActionResult>();

            var viewResult = result as RedirectToActionResult;
            viewResult.Should().NotBeNull();
            viewResult.ActionName.Should().NotBeNull();
            _mockRepo.Invocations.Should()
                .HaveCount(1)
                .And
                .Subject.Single().Method.Name
                .Should().Be("UpdateProduct");
        }

        private static void SetupMocks()
        {
            _fixture = new Fixture();
            var product = _fixture.Create<Product>();
            var products = _fixture.CreateMany<Product>();
            var suppliers = _fixture.CreateMany<Supplier>();
            var categories = _fixture.CreateMany<Category>();
            _mockRepo = new Mock<INorthWindRepository>();
            _mockRepo
                .Setup(repo => repo.GetProductAsync(Id))
                .ReturnsAsync(product);
            _mockRepo
                .Setup(repo => repo.GetProductsAsync())
                .ReturnsAsync(products);

            _mockRepo
                .Setup(repo => repo.GetSuppliersAsync())
                .ReturnsAsync(suppliers);

            _mockRepo
                .Setup(repo => repo.GetCategoriesAsync())
                .ReturnsAsync(categories);

            _mockLogger = new Mock<ILogger<ProductsController>>();
            _orchestrator = new NorthWindOrchestrator(_mockRepo.Object, _mapper);
            _controller = new ProductsController(
                _orchestrator,
                _mockLogger.Object,
                _configuration,
                _mapper);
        }

        private static void SetupConfiguration()
        {
            var inMemorySettings = new Dictionary<string, string>
            {
                {"MaximumAmountOfProducts", "2"}
            };

            _configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(inMemorySettings)
                .Build();
        }

        private static void SetupMapper()
        {
            if (_mapper == null)
            {
                var mappingConfig = new MapperConfiguration(mc =>
                {
                    mc.AddProfile(new ProductsProfile());
                    mc.AddProfile(new SuppliersProfile());
                    mc.AddProfile(new CategoriesProfile());
                });
                _mapper = mappingConfig.CreateMapper();
            }
        }
    }
}