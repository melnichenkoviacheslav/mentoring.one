using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Net.Mentoring.One.Controllers;
using Xunit;

namespace Net.Mentoring.One.UnitTests
{
    public class HomeControllerTests
    {
        [Fact]
        public async Task Index_Action_Returns_A_ViewResult()
        {
            // Arrange
            var mockLogger = new Mock<ILogger<HomeController>>();
            var controller = new HomeController(mockLogger.Object);

            // Act
            var result = await controller.Index();

            // Assert
            result.Should().NotBeNull();
            result.Should().BeOfType<ViewResult>();
        }
    }
}