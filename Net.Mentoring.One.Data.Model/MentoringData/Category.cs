﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Net.Mentoring.One.Data.Model.MentoringData
{
    [Table("categories")]
    public class Category
    {
        [Key, Column("category_id")]
        public int Id { get; set; }

        [Column("category_name")]
        public string CategoryName { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("picture")]
        public byte[] Picture { get; set; }
    }
}