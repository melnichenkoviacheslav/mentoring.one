﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Net.Mentoring.One.Data.Model.MentoringData
{
    [Table("products")]
    public class Product
    {
        [Key, Column("product_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("product_name")]
        public string ProductName { get; set; }

        [Column("supplier_id")]
        public int SupplierId { get; set; }

        [Column("category_id")]
        public int CategoryId { get; set; }

        [Column("quantity_per_unit")]
        public string QuantityPerUnit { get; set; }

        [Column("unit_price")]
        public double UnitPrice { get; set; }

        [Column("units_in_stock")]
        public int UnitsInStock { get; set; }

        [Column("units_on_order")]
        public int UnitsOnOrder { get; set; }

        [Column("reorder_level")]
        public int ReorderLevel { get; set; }

        [Column("discontinued")]
        public int Discontinued { get; set; }
    }
}