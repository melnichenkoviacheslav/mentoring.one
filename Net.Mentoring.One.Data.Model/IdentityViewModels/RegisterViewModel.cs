﻿using System.ComponentModel.DataAnnotations;

namespace Net.Mentoring.One.Data.Model.IdentityViewModels
{
    public class RegisterViewModel
    {
        [Required]
        public string Email { get; set; }

        [Required] public int Year { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Passwords are different")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        public string PasswordConfirm { get; set; }
    }
}