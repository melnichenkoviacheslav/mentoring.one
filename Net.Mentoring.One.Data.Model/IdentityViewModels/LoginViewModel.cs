﻿using System.ComponentModel.DataAnnotations;

namespace Net.Mentoring.One.Data.Model.IdentityViewModels
{
    public class LoginViewModel
    {
        [Required] 
        [Display(Name = "Email")] 
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
        
        public string ImageUrl { get; set; }
    }
}