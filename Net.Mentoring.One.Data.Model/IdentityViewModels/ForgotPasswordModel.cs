﻿using System.ComponentModel.DataAnnotations;

namespace Net.Mentoring.One.Data.Model.IdentityViewModels
{
    public class ForgotPasswordModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}