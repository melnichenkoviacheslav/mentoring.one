using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Net.Mentoring.One.Data.Repository;

var builder = WebApplication.CreateBuilder(args);

// ConfigureServices. Add services to the container. 
builder.Services.AddControllers();

builder.Services.AddDbContext<NorthWindDbContext>(opt =>
    opt.UseNpgsql(builder.Configuration.GetConnectionString("MentoringDbConnection")));

builder.Services.AddCors(options => options.AddPolicy(name: "Main", builder => builder.AllowAnyOrigin()));
builder.Services.AddOpenApiDocument();

builder.Services.AddScoped<INorthWindRepository, NorthWindRepository>();
builder.Services.AddAutoMapper(typeof(Net.Mentoring.One.Mapping.Profiles.CategoriesProfile).Assembly);


var app = builder.Build();


// Configure. Configure the HTTP request pipeline.
if (builder.Environment.IsDevelopment())
{
    app.UseOpenApi(); // serve documents (same as app.UseSwagger())
    app.UseSwaggerUi3(); // serve Swagger UI
    //app.UseReDoc(); // serve ReDoc UI
}

app.UseCors("Main");
app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();