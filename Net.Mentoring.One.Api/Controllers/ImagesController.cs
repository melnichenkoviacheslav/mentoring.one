﻿using System;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Net.Mentoring.One.Domain.NorthWindOrchestrator;

namespace Net.Mentoring.One.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ImagesController : ControllerBase
    {
        private readonly ILogger<ImagesController> _logger;
        private readonly IMapper _mapper;
        private readonly INorthWindOrchestrator _northWindOrchestrator;

        public ImagesController(
            INorthWindOrchestrator northWindOrchestrator,
            ILogger<ImagesController> logger,
            IMapper mapper)
        {
            _northWindOrchestrator =
                northWindOrchestrator ?? throw new ArgumentNullException(nameof(northWindOrchestrator));
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet("{folder}/{imageName}")]
        public async Task<ActionResult> GetImage(ImageFolders folder, string imageName)
        {
            const string contentType = "image/jpg";
            switch (folder)
            {
                case ImageFolders.Categories:
                    return base.File(await _northWindOrchestrator.GetCategoryImageAsync(imageName), contentType);
                case ImageFolders.Images:
                    var imagePath = ImageFolders.Images.ToString();
                    var image = await System.IO.File.ReadAllBytesAsync(imagePath);
                    return CreatedAtRoute(nameof(GetImage), new { folder, imageName }, base.File(image, contentType));
                default:
                    throw new ArgumentOutOfRangeException(nameof(folder), folder, $"No such folder: {folder}");
            }
        }

        [HttpPost("{folder}/{imageName}")]
        public async Task<ActionResult> GetImage(ImageFolders folder, string imageName, [FromBody] byte[] image)
        {
            const string contentType = "image/jpg";
            switch (folder)
            {
                case ImageFolders.Categories:
                    return CreatedAtRoute(nameof(GetImage), new { folder, imageName },
                        base.File(await _northWindOrchestrator.SetCategoryImageAsync(imageName, image), contentType));
                default:
                    throw new ArgumentOutOfRangeException(nameof(folder), folder, $"No such folder: {folder}");
            }
        }
    }

    public enum ImageFolders
    {
        Categories,
        Images
    }
}