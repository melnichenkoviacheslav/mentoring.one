﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Net.Mentoring.One.Data.Repository;
using Net.Mentoring.One.Domain.Model;

namespace Net.Mentoring.One.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : ControllerBase
    {
        private readonly ILogger<CategoriesController> _logger;
        private readonly IMapper _mapper;
        private readonly INorthWindRepository _northWindRepository;

        public CategoriesController(
            INorthWindRepository northWindRepository,
            ILogger<CategoriesController> logger,
            IMapper mapper)
        {
            _northWindRepository =
                northWindRepository ?? throw new ArgumentNullException(nameof(northWindRepository));
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<DomainCategory>), 200)]
        public async Task<IActionResult> GetCategories()
        {
            var categories = await _northWindRepository.GetCategoriesAsync();
            return Ok(_mapper.Map<IEnumerable<DomainCategory>>(categories));
        }
    }
}