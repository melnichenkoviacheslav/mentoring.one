﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Net.Mentoring.One.Data.Model.MentoringData;
using Net.Mentoring.One.Data.Repository;
using Net.Mentoring.One.Domain.Model;
using Net.Mentoring.One.Extensions;

namespace Net.Mentoring.One.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly ILogger<ProductsController> _logger;
        private readonly IMapper _mapper;
        private readonly INorthWindRepository _northWindRepository;

        public ProductsController(
            INorthWindRepository northWindRepository,
            ILogger<ProductsController> logger,
            IMapper mapper)
        {
            _northWindRepository =
                northWindRepository ?? throw new ArgumentNullException(nameof(northWindRepository));
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<DomainProduct>), 200)]
        public async Task<IActionResult> GetProducts()
        {
            var products = await _northWindRepository.GetProductsAsync();
            return Ok(_mapper.Map<IEnumerable<DomainProduct>>(products));
        }

        [HttpPost]
        [ProducesResponseType(typeof(DomainProduct), 200)]
        public async Task<IActionResult> AddProduct(DomainProduct inputProductModel)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState?.GetValidationErrors();
                _logger.LogError(errors);
                return BadRequest(errors);
            }

            var product = _mapper.Map<Product>(inputProductModel);
            var createdProduct = await _northWindRepository.AddProduct(product);
            var newProduct = _mapper.Map<DomainProduct>(createdProduct);

            return Ok(newProduct);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateProduct(DomainProduct inputProductModel)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState?.GetValidationErrors();
                _logger.LogError(errors);
                return BadRequest(errors);
            }

            var product = _mapper.Map<Product>(inputProductModel);

            await _northWindRepository.UpdateProduct(product);

            return NoContent();
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteProduct([FromQuery] int id)
        {
            var product = await _northWindRepository.GetProductAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            await _northWindRepository.DeleteProduct(product);

            return NoContent();
        }
    }
}