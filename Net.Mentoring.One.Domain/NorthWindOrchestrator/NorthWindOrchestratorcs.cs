﻿using AutoMapper;
using Net.Mentoring.One.Data.Repository;

namespace Net.Mentoring.One.Domain.NorthWindOrchestrator
{
    public partial class NorthWindOrchestrator : INorthWindOrchestrator
    {
        private readonly INorthWindRepository _northWindRepository;
        private readonly IMapper _mapper;

        public NorthWindOrchestrator(INorthWindRepository northWindRepository, IMapper mapper)
        {
            _northWindRepository = northWindRepository;
            _mapper = mapper;
        }
    }
}