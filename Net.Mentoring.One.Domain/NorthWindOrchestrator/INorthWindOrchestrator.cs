﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Net.Mentoring.One.Domain.Model;
using Net.Mentoring.One.View.Model.Categories;

namespace Net.Mentoring.One.Domain.NorthWindOrchestrator
{
    public interface INorthWindOrchestrator
    {
        Task<IEnumerable<DomainProduct>> GetProductsAsync();
        Task<DomainProduct> GetProductAsync(int productId);
        Task<DomainProduct> AddProduct(DomainProduct product);
        Task<IEnumerable<GetCategoryViewModel>> GetCategoriesAsync();
        Task<GetCategoryViewModel> GetCategoryAsync(int id);
        Task<IEnumerable<DomainSupplier>> GetSuppliersAsync();
        Task<DomainSupplier> GetSupplierAsync(int id);
        Task<DomainProduct> UpdateProduct(DomainProduct product);
        Task<DomainCategory> UpdateCategory(DomainCategory updatedCategory);
        Task<byte[]> GetCategoryImageAsync(string categoryId);
        Task<byte[]> SetCategoryImageAsync(string imageName, byte[] image);
    }
}