﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Net.Mentoring.One.Data.Model.MentoringData;
using Net.Mentoring.One.Domain.Model;
using Net.Mentoring.One.View.Model.Categories;

namespace Net.Mentoring.One.Domain.NorthWindOrchestrator
{
    public partial class NorthWindOrchestrator
    {
        public async Task<IEnumerable<GetCategoryViewModel>> GetCategoriesAsync()
        {
            var categories = await _northWindRepository.GetCategoriesAsync();
            return _mapper.Map<IEnumerable<GetCategoryViewModel>>(_mapper.Map<IEnumerable<DomainCategory>>(categories));
        }

        public async Task<GetCategoryViewModel> GetCategoryAsync(int id)
        {
            var category = await _northWindRepository.GetCategoryAsync(id);
            var domainCategory = _mapper.Map<DomainCategory>(category);
            return _mapper.Map<GetCategoryViewModel>(domainCategory);
        }

        public async Task<DomainCategory> UpdateCategory(DomainCategory updatedCategory)
        {
            var category = _mapper.Map<Category>(updatedCategory);
            var newCategory = await _northWindRepository.UpdateCategoryAsync(category);
            return _mapper.Map<DomainCategory>(newCategory);
        }

        public async Task<byte[]> GetCategoryImageAsync(string categoryName)
        {
            return await _northWindRepository.GetCategoryImageAsync(categoryName);
        }

        public async Task<byte[]> SetCategoryImageAsync(string imageName, byte[] image)
        {
            return await _northWindRepository.SetCategoryImageAsync(imageName, image);
        }
    }
}