﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Net.Mentoring.One.Data.Model.MentoringData;
using Net.Mentoring.One.Domain.Model;

namespace Net.Mentoring.One.Domain.NorthWindOrchestrator
{
    public partial class NorthWindOrchestrator
    {
        public async Task<IEnumerable<DomainProduct>> GetProductsAsync()
        {
            var products = await _northWindRepository.GetProductsAsync();
            return _mapper.Map<IEnumerable<DomainProduct>>(products);
        }

        public async Task<DomainProduct> GetProductAsync(int id)
        {
            var product = await _northWindRepository.GetProductAsync(id);
            return _mapper.Map<DomainProduct>(product);
        }

        public async Task<DomainProduct> AddProduct(DomainProduct domainProduct)
        {
            var product = _mapper.Map<Product>(domainProduct);
            var newProduct = await _northWindRepository.AddProduct(product);
            return _mapper.Map<DomainProduct>(newProduct);
        }

        public async Task<DomainProduct> UpdateProduct(DomainProduct domainProduct)
        {
            var product = _mapper.Map<Product>(domainProduct);
            var newProduct = await _northWindRepository.UpdateProduct(product);
            return _mapper.Map<DomainProduct>(newProduct);
        }
    }
}