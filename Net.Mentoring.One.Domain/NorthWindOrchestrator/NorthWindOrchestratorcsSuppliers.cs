﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Net.Mentoring.One.Domain.Model;

namespace Net.Mentoring.One.Domain.NorthWindOrchestrator
{
    public partial class NorthWindOrchestrator
    {
        public async Task<IEnumerable<DomainSupplier>> GetSuppliersAsync()
        {
            var suppliers = await _northWindRepository.GetSuppliersAsync();
            return _mapper.Map<IEnumerable<DomainSupplier>>(suppliers);
        }

        public async Task<DomainSupplier> GetSupplierAsync(int id)
        {
            var supplier = await _northWindRepository.GetSupplierAsync(id);
            return _mapper.Map<DomainSupplier>(supplier);
        }
    }
}