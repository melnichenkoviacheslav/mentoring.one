﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Net.Mentoring.One.View.Model.Products
{
    public class GetProductViewModel : ProductViewModel
    {
        [Required]
        public int ProductId { get; set; }

        public Dictionary<int, string> Suppliers { get; set; }

        public Dictionary<int, string> Categories { get; set; }
    }
}