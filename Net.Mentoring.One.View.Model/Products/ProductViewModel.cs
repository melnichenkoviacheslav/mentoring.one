﻿using System.ComponentModel.DataAnnotations;

namespace Net.Mentoring.One.View.Model.Products
{
    public class ProductViewModel
    {
        [Required(ErrorMessage = "Product Name can't be empty")]
        [StringLength(60, MinimumLength = 3)]
        public string ProductName { get; set; }

        [Required]
        public int SupplierId { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "Quantity PerUnit can't be empty")]
        public string QuantityPerUnit { get; set; }

        [Required(ErrorMessage = "Unit Price can't be empty")]
        [Range(0, 9999)]
        public string UnitPrice { get; set; }

        [Required(ErrorMessage = " Units In Stock can't be empty")]
        [Range(0, 9999)]
        public string UnitsInStock { get; set; }

        public int? UnitsOnOrder { get; set; }

        public int? ReorderLevel { get; set; }

        public int? Discontinued { get; set; }
    }
}