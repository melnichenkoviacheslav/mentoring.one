﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Net.Mentoring.One.View.Model.Products
{
    public class PostProductViewModel : ProductViewModel
    {
        [Required]
        public Dictionary<int, string> Suppliers { get; set; }

        [Required]
        public Dictionary<int, string> Categories { get; set; }
    }
}