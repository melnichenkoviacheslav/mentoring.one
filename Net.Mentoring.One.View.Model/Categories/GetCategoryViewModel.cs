﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Net.Mentoring.One.View.Model.Categories
{
    public class GetCategoryViewModel : CategoryViewModel
    {
        [Display(Name = "File")] 
        public IFormFile PictureFile { get; set; }
    }
}