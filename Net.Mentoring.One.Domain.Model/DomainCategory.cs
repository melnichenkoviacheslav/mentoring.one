﻿namespace Net.Mentoring.One.Domain.Model
{
    public class DomainCategory
    {
        public int Id { get; set; }

        public string CategoryName { get; set; }

        public string Description { get; set; }

        public byte[] Picture { get; set; }
    }
}