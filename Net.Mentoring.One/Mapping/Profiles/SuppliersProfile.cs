﻿using AutoMapper;
using Net.Mentoring.One.Data.Model.MentoringData;
using Net.Mentoring.One.Domain.Model;
using Net.Mentoring.One.View.Model.Suppliers;

namespace Net.Mentoring.One.Mapping.Profiles
{
    public class SuppliersProfile : Profile
    {
        public SuppliersProfile()
        {
            InitProfiles();
        }

        private void InitProfiles()
        {
            // View - Domain
            CreateMap<GetSupplierViewModel, DomainSupplier>();
            CreateMap<DomainSupplier, GetSupplierViewModel>();

            // Domain - Data
            CreateMap<Supplier, DomainSupplier>();
            CreateMap<DomainSupplier, Supplier>();
        }
    }
}