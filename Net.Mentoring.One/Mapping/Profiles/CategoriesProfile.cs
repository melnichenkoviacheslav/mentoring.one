﻿using AutoMapper;
using Net.Mentoring.One.Data.Model.MentoringData;
using Net.Mentoring.One.Domain.Model;
using Net.Mentoring.One.Extensions;
using Net.Mentoring.One.View.Model.Categories;

namespace Net.Mentoring.One.Mapping.Profiles
{
    public class CategoriesProfile : Profile
    {
        public CategoriesProfile()
        {
            InitProfiles();
        }

        private void InitProfiles()
        {
            // View - Domain
            CreateMap<GetCategoryViewModel, DomainCategory>()
                .ForMember(
                    d => d.Picture, opt => opt.MapFrom(o => o.PictureFile.GetBytes().Result)
                );
            CreateMap<DomainCategory, GetCategoryViewModel>();

            // Domain - Data
            CreateMap<Category, DomainCategory>();
            
            CreateMap<DomainCategory, Category>();
        }
    }
}