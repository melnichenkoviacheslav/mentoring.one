﻿using AutoMapper;
using Net.Mentoring.One.Data.Model.MentoringData;
using Net.Mentoring.One.Domain.Model;
using Net.Mentoring.One.View.Model.Products;

namespace Net.Mentoring.One.Mapping.Profiles
{
    public class ProductsProfile : Profile
    {
        public ProductsProfile()
        {
            InitProfiles();
        }

        private void InitProfiles()
        {
            // View - Domain
            CreateMap<GetProductViewModel, DomainProduct>();
            CreateMap<DomainProduct, GetProductViewModel>()
                .ForMember(d => d.ProductId, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.Suppliers, opt => opt.Ignore())
                .ForMember(d => d.Suppliers, opt => opt.Ignore());

            CreateMap<PostProductViewModel, DomainProduct>();
            CreateMap<ProductViewModel, DomainProduct>();
            CreateMap<DomainProduct, PostProductViewModel>();

            // Domain - Data
            CreateMap<Product, DomainProduct>();
            CreateMap<DomainProduct, Product>();
        }
    }
}