﻿using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Net.Mentoring.One.Extensions
{
    public static class ModelStateExtensions
    {
        public static string GetValidationErrors(this ModelStateDictionary modelState)
        {
            var errors = modelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).ToList();
            var aggregatedErrors = errors.Aggregate((x, y) => $"{x}\n{y}");
            return $"Invalid Post data: {aggregatedErrors}";
        }
    }
}