﻿using System.IO;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Net.Mentoring.One.Helpers.Html
{
    public static class ImageLinkHtmlHelper
    {
        public static IHtmlContent ImageLinkFor<TModel>(
            this IHtmlHelper<TModel> htmlHelper,
            string folder,
            string imageName)
        {
            var link = htmlHelper.ActionLink(
                imageName,
                "GetImage",
                "Images",
                routeValues: new
                {
                    folder, imageName
                }
            );

            using (var writer = new StringWriter())
            {
                link.WriteTo(writer, HtmlEncoder.Default);
                return new HtmlString(writer.GetStringBuilder().ToString());
            }
        }
    }
}