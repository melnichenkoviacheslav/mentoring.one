﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Net.Mentoring.One.Helpers.Tag
{
    public class ImageLinkTagHelper : TagHelper
    {
        public string ImageFolder { get; set; }

        public string ImageName { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "a";
            output.TagMode = TagMode.StartTagAndEndTag;
            output.Attributes.SetAttribute("href", $"/Images/{ImageFolder}/{ImageName}");
        }
    }
}