﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Net.Mentoring.One.Domain.Model;
using Net.Mentoring.One.Domain.NorthWindOrchestrator;
using Net.Mentoring.One.Extensions;
using Net.Mentoring.One.View.Model.Categories;

namespace Net.Mentoring.One.Controllers
{
    [Authorize]
    public class CategoriesController : Controller
    {
        private readonly ILogger<CategoriesController> _logger;
        private readonly IMapper _mapper;
        private readonly INorthWindOrchestrator _northWindOrchestrator;

        public CategoriesController(
            INorthWindOrchestrator northWindOrchestrator,
            ILogger<CategoriesController> logger,
            IMapper mapper)
        {
            _northWindOrchestrator =
                northWindOrchestrator ?? throw new ArgumentNullException(nameof(northWindOrchestrator));
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetCategories()
        {
            var categories = await _northWindOrchestrator.GetCategoriesAsync();
            return View("AllCategories", categories.OrderBy(o => o.Id));
        }

        [HttpGet]
        public async Task<IActionResult> GetCategoryAsync(int id)
        {
            var category = await _northWindOrchestrator.GetCategoryAsync(id);
            return View("GetCategory", category);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateCategory(int categoryId, GetCategoryViewModel inputCategoryViewModel)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState?.GetValidationErrors();
                _logger.LogError(errors);
                return BadRequest(errors);
            }

            var category = _mapper.Map<DomainCategory>(inputCategoryViewModel);
            category.Id = categoryId;
            await _northWindOrchestrator.UpdateCategory(category);
            return RedirectToAction(nameof(GetCategories));
        }
    }
}