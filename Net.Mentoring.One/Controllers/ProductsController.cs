﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Net.Mentoring.One.Domain.Model;
using Net.Mentoring.One.Domain.NorthWindOrchestrator;
using Net.Mentoring.One.Extensions;
using Net.Mentoring.One.View.Model.Products;

namespace Net.Mentoring.One.Controllers
{
    public class ProductsController : Controller
    {
        private readonly ILogger<ProductsController> _logger;
        private readonly IConfiguration _configuration;
        private readonly INorthWindOrchestrator _northWindOrchestrator;
        private readonly IMapper _mapper;

        public ProductsController(
            INorthWindOrchestrator northWindOrchestrator,
            ILogger<ProductsController> logger,
            IConfiguration configuration,
            IMapper mapper)
        {
            _northWindOrchestrator =
                northWindOrchestrator ?? throw new ArgumentNullException(nameof(northWindOrchestrator));
            _logger = logger;
            _configuration = configuration;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetProducts()
        {
            var maxProductsAmount = _configuration.GetValue<int>("MaximumAmountOfProducts");

            var domainProducts = await _northWindOrchestrator.GetProductsAsync();
            var domainSuppliers = await _northWindOrchestrator.GetSuppliersAsync();
            var domainCategories = await _northWindOrchestrator.GetCategoriesAsync();

            var products = _mapper.Map<IEnumerable<GetProductViewModel>>(domainProducts).ToList();
            foreach (var product in products)
            {
                product.Suppliers = domainSuppliers.ToDictionary(k => k.Id, v => v.CompanyName);
                product.Categories = domainCategories.ToDictionary(k => k.Id, v => v.CategoryName);
            }

            var filteredProducts =
                maxProductsAmount == 0
                    ? products
                    : products.Take(maxProductsAmount);
            return View(filteredProducts.OrderBy(o => o.ProductId));
        }

        [HttpGet]
        public async Task<IActionResult> GetProduct(int productId)
        {
            var domainProduct = await _northWindOrchestrator.GetProductAsync(productId);
            var domainSuppliers = await _northWindOrchestrator.GetSuppliersAsync();
            var domainCategories = await _northWindOrchestrator.GetCategoriesAsync();

            var productViewModel = _mapper.Map<GetProductViewModel>(domainProduct);

            productViewModel.Suppliers = domainSuppliers.ToDictionary(k => k.Id, v => v.CompanyName);
            productViewModel.Categories = domainCategories.ToDictionary(k => k.Id, v => v.CategoryName);

            return View("GetProduct", productViewModel);
        }

        [HttpGet]
        public async Task<IActionResult> AddProduct()
        {
            var domainSuppliers = await _northWindOrchestrator.GetSuppliersAsync();
            var domainCategories = await _northWindOrchestrator.GetCategoriesAsync();

            var postProductViewModel = new PostProductViewModel
            {
                Categories = domainCategories.ToDictionary(x => x.Id, y => y.CategoryName),
                Suppliers = domainSuppliers.ToDictionary(x => x.Id, y => y.CompanyName)
            };

            return View(postProductViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> AddProduct(ProductViewModel inputProductModel)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState?.GetValidationErrors();
                _logger.LogError(errors);
                return BadRequest(errors);
            }

            var product = _mapper.Map<DomainProduct>(inputProductModel);
            await _northWindOrchestrator.AddProduct(product);
            return RedirectToAction(nameof(GetProducts));
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProduct(int productId, GetProductViewModel inputProductModel)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState?.GetValidationErrors();
                _logger.LogError(errors);
                return BadRequest(errors);
            }

            var product = _mapper.Map<DomainProduct>(inputProductModel);
            product.Id = productId;
            await _northWindOrchestrator.UpdateProduct(product);
            return RedirectToAction(nameof(GetProducts));
        }
    }
}