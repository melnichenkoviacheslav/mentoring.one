﻿using System.Threading.Tasks;

namespace Net.Mentoring.One.Models.Email
{
    public interface IEmailSender
    {
        Task SendEmailAsync(Message message);
    }
}