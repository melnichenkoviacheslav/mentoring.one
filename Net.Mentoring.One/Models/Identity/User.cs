﻿using Microsoft.AspNetCore.Identity;

namespace Net.Mentoring.One.Models.Identity
{
    public class User : IdentityUser
    {
        public int Year { get; set; }
    }
}