using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Net.Mentoring.One.Domain.NorthWindOrchestrator;

namespace Net.Mentoring.One.Middlewares
{
    public class ImageCacheMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly string _cacheFolder;
        private readonly int _maxImagesCount;
        private readonly int _expirationTime;
        private static ConcurrentDictionary<string, DateTimeOffset> _images;

        public ImageCacheMiddleware(
            RequestDelegate next,
            IConfiguration configuration
        )
        {
            _next = next;
            _images = new ConcurrentDictionary<string, DateTimeOffset>();
            _cacheFolder = configuration["ImageCacheMiddleware:CacheFolder"];
            _maxImagesCount = int.Parse(configuration["ImageCacheMiddleware:MaxImagesCount"]);
            _expirationTime = int.Parse(configuration["ImageCacheMiddleware:ExpirationTime"]);
        }

        public async Task InvokeAsync(HttpContext context, INorthWindOrchestrator northWindOrchestrator)
        {
            var imageName = string.Empty;
            if (context.Request.Path.HasValue && context.Request.Path.Value.Contains(_cacheFolder))
            {
                imageName = context.Request.Path.Value.Split('/').LastOrDefault();

                if (imageName.Any() && _images.ContainsKey(imageName))
                {
                    if ((DateTime.UtcNow - _images[imageName].UtcDateTime).Hours < _expirationTime)
                    {
                        await context.Response.Body.WriteAsync(await ReadImage(imageName));
                        return;
                    }
                }
            }

            await _next(context);

            var responseContainsImage = context.Response.ContentType?.Contains("image");

            if (responseContainsImage != null && responseContainsImage.Value)
            {
                if (imageName.Any())
                {
                    if (!_images.ContainsKey(imageName))
                    {
                        var image = await northWindOrchestrator.GetCategoryImageAsync(imageName);

                        SaveImageOnDisk(imageName, image);

                        if (_images.Count < _maxImagesCount)
                        {
                            _images.TryAdd(imageName, DateTimeOffset.Now);
                        }
                    }
                }
            }
        }

        private async Task<byte[]> ReadImage(string fileName)
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), _cacheFolder, fileName);
            return await File.ReadAllBytesAsync(path);
        }

        private async void SaveImageOnDisk(string imageName, byte[] image)
        {
            var info = new DirectoryInfo(Directory.GetCurrentDirectory());
            if (!info.Exists)
            {
                info.Create();
            }

            var path = Path.Combine(Directory.GetCurrentDirectory(), _cacheFolder, imageName);
            await File.WriteAllBytesAsync(path, image);
        }
    }
}