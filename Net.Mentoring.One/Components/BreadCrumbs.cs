﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Net.Mentoring.One.Components
{
    public class BreadCrumbs : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(List<string> crumbs)
        {
            return View(crumbs);
        }
    }
}